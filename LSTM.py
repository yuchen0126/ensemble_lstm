import tensorflow as tf
from tensorflow.contrib import rnn
import numpy as np
# from tensorflow.examples.tutorials.mnist import input_data


# info about MINIST data set:
# Training data: 55000 images of 28 * 28 pixels each, and these 784 pixel values
# are flattened in form of a single vector of dimensionality 784. The collection of
# all such 55000 pixel vectors is stored in form of a numpy array of shape (55000, 784) - minist.train.image
# There are 10 classes in total: 0 - 9, shape (55000,10) - minist.train.label
# testing data: 10000 images
# validation data: 5000 images
from tensorflow.examples.tutorials.mnist import input_data
mnist = input_data.read_data_sets("MNIST_data/", one_hot=True)

time_steps = 28
# hidden LSTM unit
num_units = 128
# row of 28 pixels
n_input = 28
learning_rate = 0.001
n_classes = 10
batch_size = 128

out_weights= tf.Variable(tf.random_normal([num_units, n_classes]))
out_bias = tf.Variable(tf.random_normal([n_classes]))

x = tf.placeholder("float", [None, time_steps, n_input])
y = tf.placeholder("float", [None, n_classes])

# need to convert [batch_size, time_steps, n_input] to [batch_size, n_input] of length time_steps so that
# it can be feeded into static_rnn
print "x shape: ", x.shape
input = tf.unstack(x, time_steps,1)
print "input shape:", len(input), ",", input[0].shape

# define network
lstm_layer = rnn.BasicLSTMCell(num_units, forget_bias=1)
outputs, _ = rnn.static_rnn(lstm_layer, input, dtype=tf.float32)

# As only consider input of last time step
# converting last output of dimension [batch_size, num_units] to [batch_size, n_classes]
prediction = tf.matmul(outputs[-1],out_weights) + out_bias

# loss_function
loss = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits(logits=prediction, labels=y))
# optimization
opt = tf.train.AdamOptimizer(learning_rate=learning_rate).minimize(loss)

# model evaluation
correct_prediction = tf.equal(tf.argmax(prediction,1), tf.argmax(y,1))
accuracy = tf.reduce_mean(tf.cast(correct_prediction, tf.float32))

init = tf.global_variables_initializer()
with tf.Session() as sess:
    sess.run(init)
    iter = 1
    while iter < 800:
        batch_x, batch_y = mnist.train.next_batch(batch_size=batch_size)
        batch_x = batch_x.reshape((batch_size, time_steps, n_input))
        sess.run(opt, feed_dict={x: batch_x, y: batch_y})

        if iter % 80 ==0:
            acc = sess.run(accuracy, feed_dict={x: batch_x, y: batch_y})
            los = sess.run(loss, feed_dict={x: batch_x, y: batch_y})
            print("Iter: ", iter)
            print("Loss ", los)
            print("Accuracy ", acc)
            print("-------------------")
        iter += 1


test_data = mnist.test.images[:128].reshape((-1, time_steps, n_input))
test_label = mnist.test.labels[:128]
print("Testing accuracy: ", sess.run(accuracy, feed_dict={x: test_data, y: test_label}))
