import matplotlib.pyplot as plt
import tensorflow as tf
import numpy as np
from sklearn.metrics import confusion_matrix
import time
from datetime import timedelta
import math
import os
import prettytensor as pt

from tensorflow.examples.tutorials.mnist import input_data
data = input_data.read_data_sets("MNIST_data/", one_hot=True)

# labels in real number class instead of one hot
data.test.cls = np.argmax(data.test.labels, axis = 1)
data.validation.cls = np.argmax(data.validation.labels, axis = 1)
#
# print data.test.labels
# print data.test.cls

combined_images = np.concatenate([data.train.images, data.validation.images], axis = 0)
combined_labels = np.concatenate([data.train.labels, data.validation.labels], axis = 0)

print combined_images.shape
print combined_labels.shape

combined_size = combined_images.shape[0]
train_size = int(0.8 * combined_size)
validation_size = combined_size - train_size

def random_train_set():
    idx = np.random.permutation(combined_size)
    idx_train = idx[0:train_size]
    idx_validation = idx[train_size:]
    x_train = combined_images[idx_train, :]
    y_train = combined_labels[idx_train, :]
    x_validation = combined_images[idx_validation, :]
    y_validation = combined_labels[idx_validation, :]

    return x_train, y_train, x_validation, y_validation

img_size = 28
img_size_flat = img_size * img_size
img_shape = (img_size, img_size)
num_channel = 1
num_class = 10

x = tf.placeholder(tf.float32, shape=[None, img_size_flat], name='x')
x_image = tf.reshape(x, [-1, img_size, img_size, num_channel])
y_true = tf.placeholder(tf.float32, shape=[None, 10], name='y_true')
y_true_cls = tf.argmax(y_true, dimension=1)

x_pretty = pt.wrap(x_image)
with pt.defaults_scope(activation_fn=tf.nn.relu):
    y_pred, loss = x_pretty.\
        conv2d(kernel=5, depth=16, name='layer_conv1').\
        max_pool(kernel=2, stride=2).\
        conv2d(kernel=5, depth=36, name='layer_conv2').\
        max_pool(kernel=2, stride=2).\
        flatten().\
        fully_connected(size=128, name='layer_fc1').\
        softmax_classifier(num_classes=num_class, labels=y_true)

optimizer = tf.train.AdamOptimizer(learning_rate=0.0001).minimize(loss)

y_pred_cls = tf.argmax(y_pred, dimension=1)
correct_pred = tf.equal(y_pred_cls, y_true_cls)
accuracy = tf.reduce_mean(tf.cast(correct_pred, tf.float32))

# saver = tf.train.Saver(max_to_keep=100)
# save_dir = 'checkpoint/'
sess = tf.Session()
train_batch_size = 64
def random_batch(x_train, y_train):
    num_imgs = len(x_train)
    idx = np.random.choice(num_imgs, train_batch_size, replace=False)
    x_batch = x_train[idx, :]
    y_batch = y_train[idx, :]

    return x_batch, y_batch

init = tf.global_variables_initializer()
def optimize(num_iterations, x_train, y_train):
    for i in range(num_iterations):
        x_batch, y_true_batch = random_batch(x_train, y_train)
        sess.run(optimizer, feed_dict= {x: x_batch, y_true: y_true_batch})
        if i % 100 == 0:
            acc = sess.run(accuracy, feed_dict= {x: x_batch, y_true: y_true_batch})
            los = sess.run(loss, feed_dict= {x: x_batch, y_true: y_true_batch})
            print "Iteration: ", i
            print "accuracy: ", acc
            print "los: ", los

num_network = 5


if True:
    # For each of the neural networks.
    for i in range(num_network):
        print("Neural network: {0}".format(i))

        # Create a random training-set. Ignore the validation-set.
        x_train, y_train, _, _ = random_train_set()

        # Initialize the variables of the TensorFlow graph.
        sess.run(tf.global_variables_initializer())

        # Optimize the variables using this training-set.
        optimize(num_iterations=1000, x_train=x_train, y_train=y_train)

        print("------------------")




