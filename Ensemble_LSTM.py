import matplotlib.pyplot as plt
import tensorflow as tf
import numpy as np
from tensorflow.contrib import rnn
import math
import os

from tensorflow.examples.tutorials.mnist import input_data
data = input_data.read_data_sets("MNIST_data/", one_hot=True)

# labels in real number class instead of one hot
data.test.cls = np.argmax(data.test.labels, axis = 1)
data.validation.cls = np.argmax(data.validation.labels, axis = 1)
#
# print data.test.labels
# print data.test.cls

combined_images = np.concatenate([data.train.images, data.validation.images], axis = 0)
combined_labels = np.concatenate([data.train.labels, data.validation.labels], axis = 0)

print combined_images.shape
print combined_labels.shape

combined_size = combined_images.shape[0]
train_size = int(0.8 * combined_size)
validation_size = combined_size - train_size

def random_train_set():
    idx = np.random.permutation(combined_size)
    idx_train = idx[0:train_size]
    idx_validation = idx[train_size:]
    x_train = combined_images[idx_train, :]
    y_train = combined_labels[idx_train, :]
    x_validation = combined_images[idx_validation, :]
    y_validation = combined_labels[idx_validation, :]
    return x_train, y_train, x_validation, y_validation

## lstm
time_steps = 28
# hidden LSTM unit
num_units = 256
# row of 28 pixels
n_input = 28
learning_rate = 0.001
n_classes = 10
batch_size = 128
num_layers = 2

out_weights= tf.Variable(tf.random_normal([num_units, n_classes]))
out_bias = tf.Variable(tf.random_normal([n_classes]))
with tf.name_scope('inputs'):
    x = tf.placeholder("float", [None, time_steps, n_input], name="x_input")
    y = tf.placeholder("float", [None, n_classes], name="y_input")
print "x shape: ", x.shape
input = tf.unstack(x, time_steps,1)
print "input shape:", len(input), ",", input[0].shape

# dropout_1 = tf.placeholder(tf.float32) #0.5
# dropout_2 = tf.placeholder(tf.float32) #0.5

# state_placeholder = tf.placeholder(tf.float32, [num_layers, 2, batch_size, num_units])
# l = tf.unstack(state_placeholder, axis=0)
# rnn_tuple_state = tuple(
#     [tf.nn.rnn_cell.LSTMStateTuple(l[idx][0], l[idx][1])
#      for idx in range(num_layers)]
# )

# one layer
# lstm_layer = rnn.BasicLSTMCell(num_units, forget_bias=1)
# outputs, _ = rnn.static_rnn(lstm_layer, input, dtype=tf.float32)

# cell = tf.nn.rnn_cell.LSTMCell(num_units, state_is_tuple=True, forget_bias=1)
# cell = tf.nn.rnn_cell.MultiRNNCell([cell]*num_layers, state_is_tuple=True)
# outputs, state = tf.nn.dynamic_rnn(cell, input, initial_state=rnn_tuple_state,dtype=tf.float32)
with tf.variable_scope('LSTM_cell'):
    lstm_cell1 = tf.nn.rnn_cell.BasicLSTMCell(num_units, name="cell_1")
    lstm_cell2 = tf.nn.rnn_cell.BasicLSTMCell(num_units, name="cell_2")
with tf.variable_scope('dropout'):
    lstm_cell1 = tf.contrib.rnn.DropoutWrapper(lstm_cell1,input_keep_prob=1, output_keep_prob=0.5)
    lstm_cell2 = tf.contrib.rnn.DropoutWrapper(lstm_cell2,input_keep_prob=1, output_keep_prob=0.5)
with tf.variable_scope('layers'):
    lstm_layers = tf.contrib.rnn.MultiRNNCell([lstm_cell1, lstm_cell2])
# with tf.name_scope('Output'):
    outputs, _ = rnn.static_rnn(lstm_layers, input, dtype=tf.float32)

with tf.name_scope('Wx_plus_b'):
    prediction = tf.matmul(outputs[-1],out_weights) + out_bias
with tf.name_scope('Loss'):
    loss = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits(logits=prediction, labels=y))
with tf.name_scope('OPT'):
    opt = tf.train.AdamOptimizer(learning_rate=learning_rate).minimize(loss)
correct_prediction = tf.equal(tf.argmax(prediction,1), tf.argmax(y,1))
accuracy = tf.reduce_mean(tf.cast(correct_prediction, tf.float32))

saver = tf.train.Saver(max_to_keep=100)
save_dir = 'checkpoints/'
if not os.path.exists(save_dir):
    os.makedirs(save_dir)

def get_save_path(net_number):
    return save_dir + 'network' + str(net_number)


sess = tf.Session()
writer = tf.summary.FileWriter("logs/", sess.graph)

def random_batch(x_train, y_train):
    num_imgs = len(x_train)
    idx = np.random.choice(num_imgs, batch_size, replace=True)
    x_batch = x_train[idx, :]
    y_batch = y_train[idx, :]

    return x_batch, y_batch

init = tf.global_variables_initializer()
def optimize(num_iterations, x_train, y_train):
    for i in range(num_iterations):
        x_batch, y_true_batch = random_batch(x_train, y_train)
        x_batch = x_batch.reshape((batch_size, time_steps, n_input))

        sess.run(opt, feed_dict= {x: x_batch, y: y_true_batch})
        if i % 100 == 0:
            acc = sess.run(accuracy, feed_dict= {x: x_batch, y: y_true_batch})
            los = sess.run(loss, feed_dict= {x: x_batch, y: y_true_batch})
            print "Iteration: ", i
            print "accuracy: ", acc
            print "los: ", los

num_learners = 3

# model training
if True:
    for i in range(num_learners):
        print("LSTM: {0}".format(i))

        x_train, y_train, _, _ = random_train_set()
        sess.run(tf.global_variables_initializer())
        optimize(num_iterations=500, x_train=x_train, y_train=y_train)
        saver.save(sess=sess, save_path=get_save_path(i))

        print("------------------")

def predict_labels(images):
    # for larger datasets, break into batches for testing
    # num_images = len(images)
    # pred_labels = np.zeros(shape=(num_images, n_classes), dtype=np.float)
    # starting index for each batch
    # i = 0
    # while i < num_images:
    #     # The ending index for the next batch is denoted j.
    #     j = min(i + batch_size, num_images)
    #     test_img = images[i:j, :]
    #     test_img = test_img.reshape((batch_size, time_steps, n_input))
    #     pred_labels[i:j] = sess.run(prediction, feed_dict={x: test_img})
    #     i = j
    images = images.reshape((-1, time_steps, n_input))
    pred_labels = sess.run(prediction, feed_dict={x: images})

    return pred_labels

def ensemble_predictions():
    pred_label_list = []
    test_accuracies = []

    for i in range(num_learners):
        saver.restore(sess=sess, save_path=get_save_path(i))


        pred_labels = predict_labels(images=data.test.images)
        cls_pred = np.argmax(pred_labels, axis=1)
        test_acc = (data.test.cls == cls_pred).mean()
        test_accuracies.append(test_acc)
        print("LSTM: {0}, Accuracy on Test-Set: {1:.4f}".format(i, test_acc))
        pred = predict_labels(images=data.test.images)
        # print pred
        pred_label_list.append(pred)

    return np.array(pred_label_list), \
           np.array(test_accuracies)

pred_labels, test_accuracies = ensemble_predictions()

print("Mean test-set accuracy: {0:.4f}".format(np.mean(test_accuracies)))

ensemble_pred_labels = np.mean(pred_labels, axis=0)
ensemble_cls_pred = np.argmax(ensemble_pred_labels, axis=1)
ensemble_correct = (ensemble_cls_pred == data.test.cls)
print "corrent list: ", ensemble_correct
correct_number = np.sum(ensemble_correct)
print "correct num: ", correct_number
print len(data.test.cls)
test_accur = float(correct_number) / len(data.test.cls)
print "test accuracy:", test_accur
